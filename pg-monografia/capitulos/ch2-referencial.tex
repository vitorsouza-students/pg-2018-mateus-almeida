% ==============================================================================
% TCC - Mateus
% Capítulo 2 - Referencial Teórico
% ==============================================================================
\chapter{Referencial Teórico}
\label{sec-referencial}

Este capítulo apresenta os principais conceitos teóricos que fundamentaram o desenvolvimento do sistema SysMap e está organizado em 4 seções, sendo as mesmas: Engenharia de Software (\ref{sec-referencial-engenharia-software}), Especificação de Requisitos (\ref{sec-referencial-especificacao-requisisto}), Projeto de Sistemas e Implantação (\ref{sec-referencial-projeto-sistemas-implantacao}) e o método FrameWeb (\ref{sec-referencial-frameweb}).

%%% Início de seção. %%%
\section{Engenharia de Software}
\label{sec-referencial-engenharia-software}

A engenharia é aplicação de princípios matemáticos e
científicos, experiência, julgamento e bom senso para trazer coisas que beneficiam as  pessoas. A Engenharia de Software segue este mesmo raciocínio, tendo como objetivo  definir e exercitar processos, métodos, ferramentas e ambientes para construção de
software que satisfaça necessidades de cliente e usuário dentro de prazos e custos previsíveis~\cite{asee}.

O termo \emph{Engenharia} foi usado justamente para associar o ``conceito'' de \emph{Engenharia} ao desenvolvimento de software, em outras palavras, ter uma abordagem sistemática, disciplinada e quantificada ao desenvolvimento, operação e manutenção de software~\cite{ieee}.

A Engenharia de Software propõe estratégias de desenvolvimento, chamadas  modelos de ciclo de vida de desenvolvimento de software ou modelos de processo.  Estes modelos de ciclo de vida, como o nome diz, ajudam o desenvolvimento do início  ao fim do projeto. Ela se ocupa de todos os aspectos da produção de software, desde os estágios iniciais de especificação do sistema até a manutenção desse sistema, depois que ele entrou em operação~\cite{sommerville2003engenharia}.

Ela é tratada como uma ``tecnologia em camadas''~\cite{pressman1995engenharia}. Toda iniciativa de Engenharia de Software dever ser apoiada por um compromisso com a qualidade. Acima da camada da qualidade encontram-se os processos, logo acima, os métodos e, acima destes, as ferramentas. Ao longo da história da Engenharia de Software foram sendo construídas ferramentas computadorizadas para apoiar o desenvolvimento. Essas iniciativas avançaram bastante, mas ainda assim necessitam da intervenção humana. Foram concebidos vários modelos de processos de software e nenhum pode ser considerado o ideal, devido às suas divergências. Entretanto,  todos compartilham de atividades fundamentais como especificação, projeto e implementação, validação e evolução~\cite{sommerville2003engenharia}.

\section{Especificação de Requisitos}
\label{sec-referencial-especificacao-requisisto}

Resumidamente os requisitos são as funcionalidades do sistema, eles são extraídos  após o processo de análise das necessidades do clientes pela coleta de dados.

Os requisitos de um sistema são as descrições do que o sistema deve fazer, os serviços que oferece e as restrições a seu funcionamento. Esses requisitos refletem as necessidades dos clientes para um sistema que serve a uma finalidade determinada, como controlar um dispositivo, colocar um pedido ou encontrar informações. O processo de descobrir, analisar, documentar e verificar esses serviços e restrições é chamado de Engenharia de Requisitos~\cite{sommerville2003engenharia}.

As atividades de levantamento de requisitos (também conhecido como elicitação de requisitos) correspondem à etapa de compreensão do problema aplicada ao desenvolvimento de software. O principal objetivo do levantamento de requisitos é que usuário e desenvolvedores tenham a mesma visão do problema a ser resolvido. Nessa etapa, os desenvolvedores, juntamente com os clientes, tentam levantar e definir as necessidades dos futuros usuários do sistema a ser desenvolvido. Essas necessidades são geralmente denominadas requisitos~\cite{bezerrauml}.

Temos como um tipo de requisito os requisitos funcionais, que descrevem as funções que um software deverá desempenhar. Esses requisitos dependem do tipo de software que está sendo desenvolvido, dos usuários a que o software se destina e da abordagem geral considerada pela organização ao redigir os requisitos. Eles descrevem as funções do sistema detalhadamente~\cite{sommerville2003engenharia}.

Já os requisitos não funcionais descrevem as qualidades mais globais. A avaliação dos mesmos começa na fase de desenvolvimento e vai até a fase de testes finais do projeto. Os requisitos não funcionais são aqueles não diretamente relacionados às funções específicas fornecidas pelo sistema. Eles podem estar relacionados as propriedades emergentes do sistema, como confiabilidade, tempo de resposta e espaço de armazenamento~\cite{sommerville2003engenharia}.

Nessa fase é necessário um modelo que descreva o sistema em termos de suas tarefas, seu ambiente e como sistema e ambiente estão relacionados. Tal modelo deve ser passível de compreensão tanto por desenvolvedores --- analistas, projetistas, programadores e testadores --- como pela comunidade usuária --- clientes e usuários. Modelos de casos de uso (\textit{use cases}) são uma forma de estruturar essa visão~\cite{falboEngReq}.

O modelo de casos de uso é um modelo comportamental, mostrando as funções do sistema de maneira estática. Ele é composto de dois artefatos: os diagramas de casos de uso e as descrições de casos de uso. O diagrama de
casos de uso descreve graficamente o sistema, seu ambiente e como sistema e ambiente se relacionam. Assim, ele descreve o sistema segundo uma perspectiva externa. As descrições dos casos de uso descrevem o passo a passo para a realização dos casos de uso e são essencialmente
textuais. Elas tratam de como os casos de uso são realizados internamente, complementando os diagramas \cite{falboEngReq}.

Também um produto dessa fase do desenvolvimento de um software temos a modelagem conceitual estrutural, que visa modelar de forma computacional os requisitos e os casos de uso identificados anteriormente. Por meio deste modelo, é possível identificar de forma clara os tipos de entidade e os tipos de relacionamentos presentes no sistema. O propósito da modelagem conceitual estrutural, segundo o paradigma orientado a objetos, é definir as classes, atributos e associações que são relevantes para tratar o problema a ser resolvido. Para tal, as seguintes tarefas devem ser realizadas~\cite{falboEngReq}:

\begin{itemize}
	\item Identificação de Classes;
	\item Identificação de Atributos e Associações;
	\item Especificação de Hierarquias de Generalização/Especialização.
\end{itemize}

\section{Projeto de Sistemas e Implantação}
\label{sec-referencial-projeto-sistemas-implantacao}

Depois de definir o que o sistema deverá oferecer, suas funcionalidades e necessidades tem-se a fase de Projeto, que se dá durante a especificação de um software e tem por objetivo definir e especificar uma solução a ser implementada~\cite{falboProjeto}. Nesta fase se busca definir ``como'' as funcionalidades devem ser implementadas e todas as tecnologias que deverão ser usadas.

Inicialmente, o projeto é representado em um nível alto de abstração, enfocando a estrutura geral do sistema. Definida a arquitetura, o projeto passa a tratar do detalhamento de seus elementos. Esses refinamentos conduzem a representações de menores níveis de
abstração, até se chegar ao projeto de algoritmos e estruturas de  dados. Assim, independentemente do paradigma adotado, o processo de projeto envolve as seguintes atividades~\cite{falboProjeto}:

\begin{itemize}
	\item Projeto da Arquitetura do Software: Define os elementos estruturais do software e seus relacionamentos;
	\item Projeto dos Elementos da Arquitetura: Projeta em um maior nível de detalhes cada um dos elementos estruturais definidos na arquitetura, o que envolve a decomposição de módulos em outros módulos menores;
	\item Projeto Detalhado: Cuida de refinar e detalhar os elementos mais básicos da arquitetura do software, i.e., as interfaces, os procedimentos e as estruturas de dados. Deve-se descrever como se dará a comunicação entre os elementos da arquitetura (interfaces internas), a comunicação do sistema em desenvolvimento com outros sistemas (interfaces externas) e com as pessoas que vão utilizá-lo (interface com o usuário), bem como se devem projetar detalhes de algoritmos e estruturas de
	dados.
\end{itemize}

Ao final da fase de Projeto de \textit{Software}, tem-se a arquitetura na qual o sistema será montado juntamente com o projeto dos componentes do sistema. Esses componentes, como proposto por \citeonline{fowler2002}, podem ser classificados da seguinte forma: 

\begin{itemize}
	\item Interface com o Usuário: componente responsável por gerenciar todo o contato do usuário com o sistema, seja para receber dados quanto para apresentar dados;
	\item Lógica de Negócio: encarregado de aplicar todas as regras de negócios e requisitos do sistema. É uma camada intermediária de um sistema, que recebe os dados vindo da interface com o usuário e após
	aplicada a lógica de negócio repassa os dados para a camada seguinte;
	\item Persistência de Dados: guarda os dados de forma a ser possível recuperá-los quando 	necessário em um sistema de banco de dados integrando com o sistema.
\end{itemize}

%%% Início de seção. %%%
\section{O método FrameWeb}
\label{sec-referencial-frameweb}

O FrameWeb é um método para projetar \textit{WebApps}, onde se assume o uso de certos tipos de \textit{frameworks} durante o processo de software. Ele define uma arquitetura básica para \textit{WebApps} e propõe modelos mais próximos sua implementação usando esses \textit{frameworks}~\cite{vitorFalvoFrameWeb}. Essa arquitetura é baseada no padrão Camada de Serviço~\cite{fowler2002}. Tal arquitetura é organizada em camadas como na Figura~\ref{fig-arquitetura_FrameWeb}  e descritas a seguir~\cite{martins-mestrado15}:


\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\textwidth]{figuras/cap2_Arquitetura_FrameWeb}
	\caption{Arquitetura padrão para Sistema de Informação \textit{Web} baseada no padrão arquitetônico \textit{Service Layer} \cite{fowler2002}.}
	\label{fig-arquitetura_FrameWeb}
\end{figure}

\begin{itemize}
	
	\item Camada de Apresentação (\textit{Presentation Tier}): que diz respeito às interfaces gráficas do usuário. Dividida em Visão (\textit{View}), que contém páginas da Web, imagens, modelos de layout e outros arquivos relacionados exclusivamente com
	a exposição de informações para o usuário; e Controle (\textit{Control}), que compreende classes de ação e outros arquivos relacionados ao \textit{framework} MVC;
	
	\item Camada de Negócio (\textit{Business Tier}): 
	onde a lógia de negócio é implementada. Dividida em
	Lógica de Domínio (\textit{Domain}), que inclui os conceitos de domínio de negócios identificados e modelados por diagramas de classes durante a Análise e refinados durante o Projeto; e Lógica de Aplicação (\textit{Application}), que implementa os casos de uso definidos na especificação de requisitos;
	
	\item Camada de Acesso a Dados (\textit{Data Access Tier}): que é responsável por armazenar os objetos persistentes em mídia de longa duração, como por exemplo com o uso de bancos de dados ou arquivos.
\end{itemize}

Sendo um método para a fase de Projeto, ele não prescreve um processo de software completo. Além da definição de uma arquitetura padrão apresentada acima, o método propõe um conjunto de modelos de projeto que trazem conceitos utilizados pelos \textit{frameworks} para esta fase do processo, deixando assim os diagramas mais próximos da implementação~\cite{vitorFrameWeb}.

O método FrameWeb está focado na fase de projeto e originalmente definia uma linguagem de modelagem baseada em extensões leves (lightweight extensions) ao metamodelo da UML, com a finalidade de criar um perfil UML para ser utilizado pelo método e que serve para a construção de diagramas. 

Entretanto, a evolução do FrameWeb proposta por \citeonline{martins-mestrado15} define uma linguagem Específica de Domínio (\textit{Domain Specific Language} ou DSL) formal para o FrameWeb, por meio de metamodelos que estendem uma parte do metamodelo da UML. Tais metamodelos especificam a sintaxe abstrata da linguagem, descrevendo os conceitos e as regras de modelagem específicas do método e, consequentemente, permitem a criação de ferramentas e mecanismos para que esta linguagem possa ser utilizada de forma correta e direcionada~\cite{matos2017}.
  
Tem-se seguir um resumo dos modelos propostos no FrameWeb, todos eles baseados no diagrama de classes da UML:

\begin{itemize}
	\item \textbf{Modelo de Entidades (\textit{Entity Model})}, referente ao pacote de Domínio da aplicação, para representar o domínio do problema da aplicação Web e o mapeamento desses objetos para a persistência;
	\item \textbf{Modelo de Persistência (\textit{Persistence Model})}, para representar os objetos persistentes
	utilizando o padrão de projeto DAO~\cite{alur}, referente aos elementos contidos no pacote de Persistência;
	\item \textbf{Modelo de Navegação (\textit{Navigation Model})}, para representar os diversos componentes
	contidos no pacote de Visão e Controle, que formam a interface com o usuário, a partir da qual é possível interagir com a lógica de negócio;
	\item \textbf{Modelo de Aplicação (\textit{Application Model})}, para representar as estruturas de
	serviço pertencentes ao pacote de Aplicação, que são responsáveis pela implementação dos casos de uso, bem como as dependências de/para componentes de outras camadas (Controle e Persistência).
\end{itemize}



